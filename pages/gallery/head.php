<script src="/js/jquery.js"></script>
<script src="/js/slider.js"></script>
<script>
    $(function () {

        var slider = new $.Slider();

        $('.column img').click(function () {
            $('.slider').fadeIn(200);
        });

        $('.slider').click(function () {
            $('.slider').fadeOut(200);
        });

        $('.slide').click(function () {
            return false;
        });

        $('.galleryWrapper img').click(function () {
            slider.setPhotoByURL(this.src);
        });

        $('.slide')
            .mouseenter(function () {
                $('.close').fadeOut(100);
            })
            .mouseleave(function () {
                $('.close').fadeIn(100);
            })
        ;



    });
</script>