<div slider="allPhotos" style="display: none;">
    <img mainPhoto="true" src="/photo_folder/1.jpg"/>
    <img src="/photo_folder/2.jpg"/>
    <img src="/photo_folder/3.jpg"/>
    <img src="/photo_folder/4.jpg"/>
    <img src="/photo_folder/5.jpg"/>
    <img src="/photo_folder/6.jpg"/>
    <img src="/photo_folder/7.jpg"/>
    <img src="/photo_folder/8.jpg"/>
    <img src="/photo_folder/9.jpg"/>
    <img src="/photo_folder/10.jpg"/>
    <img src="/photo_folder/11.jpg"/>
    <img src="/photo_folder/12.jpg"/>
    <img src="/photo_folder/13.jpg"/>
    <img src="/photo_folder/14.jpg"/>
    <img src="/photo_folder/15.jpg"/>
    <img src="/photo_folder/16.jpg"/>
    <img src="/photo_folder/17.jpg"/>
    <img src="/photo_folder/18.jpg"/>
    <img src="/photo_folder/19.jpg"/>
    <img src="/photo_folder/20.jpg"/>
</div>

<div class="galleryWrapper">
    <div class="column">
        <img src="/photo_folder/1.jpg"/>
        <img src="/photo_folder/2.jpg"/>
        <img src="/photo_folder/3.jpg"/>
        <img src="/photo_folder/4.jpg"/>
        <img src="/photo_folder/5.jpg"/>
    </div>
    <div class="column">
        <img src="/photo_folder/6.jpg"/>
        <img src="/photo_folder/7.jpg"/>
        <img src="/photo_folder/8.jpg"/>
        <img src="/photo_folder/9.jpg"/>
        <img src="/photo_folder/10.jpg"/>
    </div>
    <div class="column">
        <img src="/photo_folder/11.jpg"/>
        <img src="/photo_folder/12.jpg"/>
        <img src="/photo_folder/13.jpg"/>
        <img src="/photo_folder/14.jpg"/>
        <img src="/photo_folder/15.jpg"/>
    </div>
    <div class="column">
        <img src="/photo_folder/16.jpg"/>
        <img src="/photo_folder/17.jpg"/>
        <img src="/photo_folder/18.jpg"/>
        <img src="/photo_folder/19.jpg"/>
        <img src="/photo_folder/20.jpg"/>
    </div>
    <div style="clear: both"></div>
</div>
<div class="slider" slider="slider" style="display: none;">
    <div class="close" style="display: none;"></div>
    <div class="slide" slider="slide">
        <table class="navigation">
            <tr>
                <td class="prevBtn" slider="prevBtn">
                    <img src="img/arrow_left.png"/></td>
                <td class="nextBtn" slider="nextBtn">
                    <img src="img/arrow_right.png"/></td>
            </tr>
        </table>
    </div>
</div>