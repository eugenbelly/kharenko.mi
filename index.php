<? $page = $_REQUEST['page']; ?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="/style/style.css"/>
        <link rel="stylesheet" href="/style/<?=$_REQUEST['page']?>.css"/>
        <?require('/pages/'.$page.'/head.php');?>
    </head>
    <body>
        <header>
            <a href="/?page=home" class="logo"></a>
        </header>
        <div class="content">
            <nav class="menu">
                <b><a class="menuItem
                    <?if ($page === 'home'): ?> active <?endif?>"
                    href="/?page=home">HOME</a></b><br/>

                <b><a class="menuItem
                    <?if ($page === 'gallery'): ?> active <?endif?>"
                    href="/?page=gallery">GALLERY</a></b><br/>

                <b><a class="menuItem
                    <?if ($page === 'shop'): ?> active <?endif?>"
                    href="#">SHOP</a></b><br/>
            </nav>
            <article>
                <?require('/pages/'.$page.'/content.php');?>
            </article>
        </div>
        <footer>
            <a href="#" class="copyright">(c) Cool Guys Studio 2015</a>
            <div class="socialButtons">
                <a href="#" class="vk"></a>
                <a href="#" class="twitter"></a>
                <a href="#" class="instagram"></a>
            </div>
        </footer>
    </body>
</html>
<!--TODO: добавить полупрозрачные белые кругляши к стрелкам-->